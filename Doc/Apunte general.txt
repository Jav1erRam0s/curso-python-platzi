********************************************************************************************************************************
* Mod.2 - Uso de strings y ciclos
********************************************************************************************************************************

* Los Strings sos inmutables.
* Slices (rebanadas) se consigue con esta sintaxis ':'
* Bucle For
    - break : para salir del bucle
    - continue : para pasar a la siguiente iteracion
* Bucle While
    - break : para salir del bucle

********************************************************************************************************************************
* Mod.3 - Estructuras de datos
********************************************************************************************************************************

* Diccionarios
    - Se usa llave para acceder al valor.

* Tuplas
    - Son como las listas, solo que son inmutables.
    - Se puede acceder al igual que las listas, con el indice.

* Sets
    - No se puede repetir elemenetos.
    - Permite realizar operaciones de conjunto union, interseccion, diferencia.

* List comprehensions y Dictionary comprehensions
    - Permite crear una lista o diccionario de una forma mas corta de escribir. Ver ejemplo List_Dictionary_comprehensions.py.

********************************************************************************************************************************
* Mod.5 - Proyecto Web Scraping
********************************************************************************************************************************

* Web Scraping
    - Se trata de un proceso de usar bots para extraer contenido y datos de un sitio web. De esta forma se extrae el código 
    HTML y con él, los datos.

********************************************************************************************************************************
* Mod.7 - Proyecto Aplicacion Web
********************************************************************************************************************************

* Flask
    - Es un framework minimalista escrito en Python que permite crear aplicaciones web rápidamente y con un mínimo número de 
    líneas de código. Está basado en la especificación WSGI de Werkzeug y el motor de templates Jinja2 y tiene una licencia BSD.
* Jinja2
    - Es el lenguaje por default que utiliza flask.
