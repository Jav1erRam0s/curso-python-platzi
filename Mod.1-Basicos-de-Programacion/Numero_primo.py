
def is_prime(number):
    if number < 2:
        return False
    elif number == 2:
        return True
    elif number > 2 and number % 2 == 0:
        return False
    else:
        for i in range(3, number):
            if number % i == 0:
                return False
    return True

def run():
    print('¿ES PRIMO?')

    number = int(input('Ingresa el numero a evaluar: '))
    result = is_prime(number)
    if result:
        print('Es numero primo.')
    else:
        print('No es numero primo.')

if __name__ == '__main__':
    run()
    