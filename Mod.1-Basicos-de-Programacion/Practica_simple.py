
# Operaciones

print (5*5)
print (5**3)
print (5/2)
print (5+5)
print (5-2)

# Tipo de datos

print(type(5))
print(type(5.2))
print(type(False))
print(type('Javier'))

# Calculo de area
pi = 3.14159
radio = 3
area = pi * radio**2
print(area)

# Saludo
name = str(input('¿Cúal es tu nombre?'))
print('Hola, ' + name + '!')
