
# Primer dicccionario
mi_diccionario = {}
mi_diccionario['primer_elemento'] = 'Hola'
mi_diccionario['segundo_elemento'] = 'Adios'
print(mi_diccionario['primer_elemento'])

# Datos
calificaciones = {}
calificaciones['Matematica'] = 9
calificaciones['Base de Datos'] = 10
calificaciones['Web'] = 8
calificaciones['Algoritmos'] = 10

# Iteracion de diccinario

# Iteracion de llaves
for key in calificaciones:
    print(key)

# Iteracion de valor
for value in calificaciones.values():
    print(value)

# Iteracion de llaves y valor
for key, value in calificaciones.items():
    print( 'Llave : {}, valor : {}'.format(key,value) )

# Calicular suma y promedio
sum = 0
for value in calificaciones.values():
    sum += value

print('Suma : {}'.format(sum) )
print('Promedio : {}'.format(sum/len(calificaciones)) )
