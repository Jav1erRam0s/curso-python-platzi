import csv
from Contact import Contact

class ContactBook:

    # La notacion '_' en los atributos indica que es una propiedad privada.

    def __init__(self):
        self._contacts = []

    def add(self, name, phono, email):
        contact = Contact(name, phono, email)
        self._contacts.append(contact)
        self._save()

    def show_all(self):
        for contact in self._contacts:
            self._print_contact(contact)

    def _print_contact(self, contact):
        print('* --- * --- * --- * --- * --- * --- * --- * --- * --- *')
        print('Nombre : {}'.format(contact.name))
        print('Telefono : {}'.format(contact.phono))
        print('Email : {}'.format(contact.email))
        print('* --- * --- * --- * --- * --- * --- * --- * --- * --- *')

    def _not_found():
        print('*******')
        print('¡No encontrado!')
        print('*******')

    def delete(self, name):
        for idx, contact in enumerate(self._contacts):
            if contact.name.lower() == name.lower():
                del self._contacts[idx]
                self._save()
                break
        else:
            self._not_found()

    def search(self, name):
        for contact in self._contacts:
            if contact.name == name:
                self._print_contact(contact)
                break
    def update(self, name):
        for idx, contact in enumerate(self._contacts):
            if contact.name.lower() == name.lower():
                name = str(input('Ingrese el nuevo nombre del contacto : '))
                phono = str(input('Ingrese el nuevo telefono del contacto : '))
                email = str(input('Ingrese el nuevo email del contacto : '))
                self._contacts[idx].name = name
                self._contacts[idx].phono = phono
                self._contacts[idx].email = email
                self._save()
                break
        else:
            self._not_found()

    def _save(self):
        with open('contacts.csv', 'w') as f:
            writer = csv.writer(f)
            writer.writerow( ('name', 'phono', 'email') )

            for contact in self._contacts:
                writer.writerow( (contact.name, contact.phono, contact.email) )
