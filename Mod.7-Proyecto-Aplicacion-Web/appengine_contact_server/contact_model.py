from google.appengine.ext import ndb

class Contact(ndb.Model):
    name = ndb.StringProperty()
    phono = ndb.StringProperty()
    email = ndb.StringProperty()
